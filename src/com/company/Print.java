package com.company;

public class Print {

    synchronized void printText(String values){
        for (int i = 0; i < values.length(); i++) {
            System.out.print(values.charAt(i));
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
